package course.examples.practica_05;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Main2Activity extends AppCompatActivity {

    ImageView image1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        image1 = (ImageView) findViewById(R.id.imageView1);
        int nImg = Integer.parseInt(message);

        if(nImg==1){
            image1.setImageResource(R.drawable.manzanab);
        }else if(nImg==2){
            image1.setImageResource(R.drawable.pinab);
        }else if(nImg==3){
            image1.setImageResource(R.drawable.sandia);
        }else{
            image1.setImageResource(R.drawable.uva);
        }

    }

}
