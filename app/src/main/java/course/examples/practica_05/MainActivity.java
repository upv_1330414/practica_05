package course.examples.practica_05;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.view.View.OnClickListener;

public class MainActivity extends AppCompatActivity {

    ImageView image1;
    Button button1;
    Button button2;
    Button button3;
    Button button4;

    public final static String EXTRA_MESSAGE = "course.examples.practica_05.MESSAGE";
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addListenerOnButton();
    }

    public void addListenerOnButton() {

        intent = new Intent(this, Main2Activity.class);
        image1 = (ImageView) findViewById(R.id.imageView1);

        button1 = (Button) findViewById(R.id.btnChangeImage1);
        button1.setOnClickListener(new OnClickListener() {

            @Override public void onClick(View arg0) {
                intent.putExtra(EXTRA_MESSAGE, "1");
                startActivity(intent);
            }
        });

        button2 = (Button) findViewById(R.id.btnChangeImage2);
        button2.setOnClickListener(new OnClickListener() {

            @Override public void onClick(View arg0) {
                intent.putExtra(EXTRA_MESSAGE, "2");
                startActivity(intent);
            }
        });

        button3 = (Button) findViewById(R.id.btnChangeImage3);
        button3.setOnClickListener(new OnClickListener() {

            @Override public void onClick(View arg0) {
                intent.putExtra(EXTRA_MESSAGE, "3");
                startActivity(intent);
            }
        });

        button4 = (Button) findViewById(R.id.btnChangeImage4);
        button4.setOnClickListener(new OnClickListener() {

            @Override public void onClick(View arg0) {
                intent.putExtra(EXTRA_MESSAGE, "4");
                startActivity(intent);
            }
        });
    }



}
